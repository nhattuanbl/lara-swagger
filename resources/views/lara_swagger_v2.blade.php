<!doctype html>
<html>
<head>
    <title>{{ \Illuminate\Support\Facades\Config::get('app.name') }}</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
</head>
<body>
<script
    id="api-reference"
{{--    data-url="https://petstore3.swagger.io/api/v3/openapi.json"--}}
    data-url="{{ route('docs.show') }}"
{{--    data-proxy-url="https://api.scalar.com/request-proxy"--}}
></script>
<script>
    var configuration = {
        theme: 'purple',
    }

    var apiReference = document.getElementById('api-reference')
    apiReference.dataset.configuration = JSON.stringify(configuration)
</script>
<script
{{--    src="https://cdn.jsdelivr.net/npm/@scalar/api-reference"--}}
    src="{{ asset('vendor/lara_swagger/js/api-reference@1.24.31.standalone.js') }}"
></script>
<script>
    let scalarCheck = setInterval(function () {
        let tokenInputs = document.querySelectorAll('#security-scheme-Authorization')
        if (tokenInputs) {
            // clearInterval(scalarCheck);
            for (let i = 0; i < tokenInputs.length; i++) {
                tokenInputs[i].setAttribute("type", "text");
            }
        }
    }, 1500);


</script>
</body>
</html>
