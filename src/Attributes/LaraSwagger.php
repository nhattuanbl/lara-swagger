<?php

namespace LaraSwagger\Attributes;

use Illuminate\Support\Collection;

#[\Attribute(\Attribute::TARGET_CLASS)]
class LaraSwagger
{
    public string $name;
    public ?array $properties = [];

    /** @var null|Collection<Parameter> */
    public ?Collection $filters = null;

    public function __construct(
        public ?string $controller = null,
        public ?string $tag = null,
        public ?array $hidden = [],
        public ?array $readOnly = [],
        public string|array|null $description = null,
    )
    {

    }

    public function initResources(\LaraSwagger\LaraSwagger $laraSwagger)
    {
        if (!class_exists($this->controller)) {
            return;
        }

        $refl = new \ReflectionClass($this->controller);
        if ($refl->hasMethod('index') && $refl->getMethod('index')->isPublic()) {
            $laraSwagger->operations->push([
                'method' => 'get',
                'endpoint' => '/' . $this->name,
                'swagger' => [
                    'tags' => [$this->tag],
                    'summary' => 'Display a listing of the resource.',
                    'description' => is_string($this->description) ? $this->description : ($this->description['get'] ?? null),
                    'parameters' => $this->getParameters($this->filters),
                    'responses' => [
                        200 => ['$ref' => '#/components/responses/Pagination' . $this->tag],
//                        400 => ['$ref' => '#/components/responses/BadRequest'],
//                        401 => ['$ref' => '#/components/responses/Unauthorized'],
//                        403 => ['$ref' => '#/components/responses/Forbidden'],
//                        404 => ['$ref' => '#/components/responses/NotFound'],
//                        405 => ['$ref' => '#/components/responses/NotAllowed'],
//                        500 => ['$ref' => '#/components/responses/ServerError'],
                    ]
                ]
            ]);

            $laraSwagger->schemas->push([
                'index' => $this->tag,
                'swagger' => [
                    'type' => 'object',
                    'properties' => $this->getProperties()
                ]
            ]);

            $laraSwagger->responses->push([
                'index' => 'Pagination' . $this->tag,
                'swagger' => [
                    'description' => 'Listing of the **' . $this->tag . '** resources',
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'data' => [
                                        'type' => 'array',
                                        'items' => ['$ref' => '#/components/schemas/' . $this->tag]
                                    ],
                                    'path' => [
                                        'type' => 'string',
                                        'example' => config('app.url') . $laraSwagger->baseUrl . '/' . $this->name,
                                    ],
                                    'first_page_url' => [
                                        'type' => 'string',
                                        'example' => config('app.url') . $laraSwagger->baseUrl . '/' . $this->name . '?page=1',
                                    ],
                                    'last_page_url' => [
                                        'type' => 'string',
                                        'example' => config('app.url') . $laraSwagger->baseUrl . '/' . $this->name . '?page=50',
                                    ],
                                    'prev_page_url' => [
                                        'type' => 'string',
                                        'example' => null,
                                    ],
                                    'next_page_url' => [
                                        'type' => 'string',
                                        'example' => config('app.url') . $laraSwagger->baseUrl . '/' . $this->name . '?page=2',
                                    ],
                                    'from' => [
                                        'type' => 'integer',
                                        'example' => 1,
                                    ],
                                    'to' => [
                                        'type' => 'integer',
                                        'example' => 20,
                                    ],
                                    'per_page' => [
                                        'type' => 'integer',
                                        'example' => 20,
                                    ],
                                    'current_page' => [
                                        'type' => 'integer',
                                        'example' => 1,
                                    ],
                                    'last_page' => [
                                        'type' => 'integer',
                                        'example' => 50,
                                    ],
                                    'total' => [
                                        'type' => 'integer',
                                        'example' => 1000,
                                    ],
                                ]
                            ]
                        ]
                    ]
                ]
            ]);
        }

        if ($refl->hasMethod('store') && $refl->getMethod('store')->isPublic()) {
            $laraSwagger->operations->push([
                'method' => 'post',
                'endpoint' => '/' . $this->name,
                'swagger' => [
                    'tags' => [$this->tag],
                    'summary' => 'Store a newly created resource in storage.',
                    'description' => $this->description['post'] ?? null,
                    'requestBody' => [
                        'description' => $this->tag . ' that should be stored',
                        'required' => true,
                        'content' => [
                            'application/json' => ['schema' => ['$ref' => '#/components/schemas/' . $this->tag]]
                        ]
                    ],
                    'responses' => [
                        201 => [
                            'description' => 'Created',
                            'content' => [
                                'application/json' => [
                                    'schema' => [
                                        '$ref' => '#/components/schemas/' . $this->tag
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]);
        }

        if ($refl->hasMethod('show') && $refl->getMethod('show')->isPublic()) {
            $laraSwagger->operations->push([
                'method' => 'get',
                'endpoint' => '/' . $this->name . '/{id}',
                'swagger' => [
                    'tags' => [$this->tag],
                    'summary' => 'Display the specified resource.',
                    'description' => $this->description['get'] ?? null,
                    'parameters' => [
                        [
                            'name' => 'id',
                            'in' => 'path',
                            'description' => 'id of ' . strtolower($this->name),
                            'required' => true,
                            'schema' => ['type' => 'integer']
                        ]
                    ],
                    'responses' => [
                        200 => [
                            'description' => 'Ok',
                            'content' => [
                                'application/json' => [
                                    'schema' => [
                                        '$ref' => '#/components/schemas/' . $this->tag
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]);
        }

        if ($refl->hasMethod('update') && $refl->getMethod('update')->isPublic()) {
            $laraSwagger->operations->push([
                'method' => 'put',
                'endpoint' => '/' . $this->name . '/{id}',
                'swagger' => [
                    'tags' => [$this->tag],
                    'summary' => 'Update the specified resource in storage.',
                    'description' => $this->description['put'] ?? null,
                    'parameters' => [
                        [
                            'name' => 'id',
                            'in' => 'path',
                            'description' => 'id of ' . strtolower($this->name),
                            'required' => true,
                            'schema' => ['type' => 'integer']
                        ]
                    ],
                    'requestBody' => [
	                    'description' => $this->tag . ' that should be updated',
	                    'required' => true,
	                    'content' => [
		                    'application/json' => ['schema' => ['$ref' => '#/components/schemas/' . $this->tag]]
	                    ]
                    ],
                    'responses' => [
                        200 => [
                            'description' => 'Ok',
                            'content' => [
                                'application/json' => [
                                    'schema' => [
                                        '$ref' => '#/components/schemas/' . $this->tag
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]);
        }

        if ($refl->hasMethod('destroy') && $refl->getMethod('destroy')->isPublic()) {
            $laraSwagger->operations->push([
                'method' => 'delete',
                'endpoint' => '/' . $this->name . '/{id}',
                'swagger' => [
                    'tags' => [$this->tag],
                    'summary' => 'Remove the specified resource from storage.',
                    'description' => $this->description['delete'] ?? null,
                    'parameters' => [
                        [
                            'name' => 'id',
                            'in' => 'path',
                            'description' => 'id of ' . strtolower($this->name),
                            'required' => true,
                            'schema' => ['type' => 'integer']
                        ]
                    ],
                    'responses' => [
                        204 => ['$ref' => '#/components/responses/Deleted'],
                    ]
                ]
            ]);
        }
    }

    public function getProperties(): array
    {
        $result = [];
        foreach ($this->properties as $property => $type) {
//            $result[$property]['type'] = $type;

            if ($property == 'email') {
                $result[$property]['format'] = 'email';
            }

            $types = explode('|', $type);
            foreach ($types as $item) {
                if ($item == 'datetime' || $item == 'Carbon' || str_starts_with($item, 'datetime:')) {
                    $result[$property]['type'] = 'string';
                    $result[$property]['format'] = 'date-time';
                    $result[$property]['example'] = date('Y-m-d H:i:s');
                } else if ($item == 'date') {
                    $result[$property]['type'] = 'string';
                    $result[$property]['format'] = 'date';
                    $result[$property]['example'] = date('Y-m-d');
                } else if ($item == 'array' || $item == 'Collection' || str_ends_with($item, '[]')) {
                    $result[$property]['type'] = 'array';
                    if (str_ends_with($item, '[]')) {
                        $result[$property]['items'] = ['type' => 'string'];

                        if($item == 'integer[]' || $item == 'int[]') {
                            $result[$property]['items'] = ['type' => 'integer'];
                        }
                    }
                } else if ($item == 'int' || $item == 'integer') {
                    $result[$property]['type'] = 'integer';
                    $result[$property]['format'] = 'int32';
                } else if ($item == 'boolean') {
                    $result[$property]['type'] = 'boolean';
                } else if ($item == 'float' || $item == 'double') {
                    $result[$property]['type'] = 'number';
                    $result[$property]['format'] = 'double';
                }
            }

            if (!isset($result[$property]['type'])) {
                $result[$property]['type'] = 'string';
            }

            if (in_array($property, $this->readOnly)) {
                $result[$property]['readOnly'] = true;
            }
        }

        return $result;
    }

    public static function getParameters(Collection|Parameter|null $parameters = null): array
    {
        if($parameters instanceof Collection || is_array($parameters)) {
            $result = [];
            foreach ($parameters as $parameter) {
                if (!$parameter) {
                    continue;
                }

                $result[] = self::getParameters($parameter);
            }

            return $result;
        }

        if ($parameters->ref) {
            return ['$ref' => $parameters->ref];
        }

        $p = [
            'name' => $parameters->name,
            'in' => $parameters->in,
            'description' => $parameters->description,
            'required' => $parameters->required,
            'schema' => ['type' => $parameters->type]
        ];

        if ($parameters->default) {
            $p['default'] = $parameters->default;
        }

        if ($parameters->type == 'boolean') {
            $p['default'] = (bool) $parameters->default;
        }
        if ($parameters->minimum) $p['schema']['minimum'] = $parameters->minimum;
        if ($parameters->maximum) $p['schema']['maximum'] = $parameters->maximum;
        if ($parameters->enum) $p['schema']['enum'] = $parameters->enum;

        return $p;
    }
}
