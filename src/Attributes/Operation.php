<?php

namespace LaraSwagger\Attributes;

#[\Attribute(\Attribute::TARGET_METHOD)]
class Operation
{
    public function __construct(
        public string $endpoint,
        public string $method,

        public ?string $tag = null,
        public ?string $summary = null,
        public ?string $description = null,
    )
    {

    }
}
