<?php

namespace LaraSwagger\Attributes;

#[\Attribute(\Attribute::IS_REPEATABLE | \Attribute::TARGET_METHOD | \Attribute::TARGET_CLASS)]
class Parameter
{
    public function __construct(
        public string $name,
        public string $type,
        public string $in = 'query',
        public ?string $ref = null,
        public ?array $enum = [],
        public ?bool $required = false,
        public string|int|null $default = null,
        public ?string $description = null,
        public ?int $minimum = null,
        public ?int $maximum = null,
    )
    {

    }
}
