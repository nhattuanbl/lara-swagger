<?php

namespace LaraSwagger\Attributes;

#[\Attribute(\Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class RequestBody
{
    public function __construct(
        public string $name,
        public string $type,
        public null|string|int|float $example = null,
        public bool $required = false,
        public string $contentType = 'application/json', //multipart/form-data
        public ?string $desc = null,
    )
    {

    }
}
