<?php

namespace LaraSwagger\Attributes;

#[\Attribute(\Attribute::IS_REPEATABLE | \Attribute::TARGET_METHOD)]
class Response
{
    public function __construct(
        public string|int $code,
        public ?string $description = null,
        public ?string $ref = null,

        public ?array $content = [],
    )
    {

    }
}
