<?php

namespace LaraSwagger;

use App\Http\Controllers\Controller;
use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use LaraSwagger\Attributes\Operation;
use LaraSwagger\Attributes\Parameter;
use LaraSwagger\Attributes\RequestBody;
use LaraSwagger\Attributes\Response;
use Roave\BetterReflection\Reflection\ReflectionMethod;
use Symfony\Component\Finder\SplFileInfo;

class LaraSwagger
{
    public string $baseUrl;
    public bool $laravel_route_name = false;
    public Collection $laraSwagger;
    public Collection $operations;
    public Collection $responses;
    public Collection $schemas;

    public Collection $models;
    public Collection $controllers;

    public array $info = [
        'title' => 'LaraSwagger APIs',
        'version' => '1.0.0',
        'contact' => [
            'email' => 'nhattuanbl@gmail.com'
        ]
    ];

    public array $security = [];

    public mixed $callbackOperation = null;

    public function __construct()
    {
        $this->laraSwagger = new Collection();
        $this->operations = new Collection();
        $this->responses = new Collection();
        $this->schemas = new Collection();

        $this->models = new Collection();
        $this->controllers = new Collection();
    }

    public function getDocs(array $servers = [['url' => '/api']])
    {
        $this->models = self::getAllModels();
        $this->controllers = self::getAllController();
        $this->baseUrl = $servers[0]['url'];

        $this->_setModels()->_setControllers();

        return [
            'openapi' => '3.0.0',
            'info' => $this->info,
            'servers' => $servers,
            'paths' => $this->_getOperations(),
            'components' => [
                'schemas' => $this->_getSchemas(),
                'responses' => $this->_getResponses(),
                'securitySchemes' => $this->security,
            ]
        ];
    }

    public static function getAllModels(): Collection
    {
        return collect(File::allFiles(app_path('Models')))
            ->map(function (SplFileInfo $splFileInfo) {
                $path = $splFileInfo->getRelativePathName();
                $namespace = sprintf('\%s%s', Container::getInstance()->getNamespace() . 'Models\\',
                    strtr(substr($path, 0, strrpos($path, '.')), '/', '\\'));

                if (class_exists($namespace)) {
                    $reflection = new \ReflectionClass($namespace);
                    if ($reflection->isSubclassOf(Model::class) && !$reflection->isAbstract()) {
                        return $reflection;
                    }
                }

                return null;
            })
            ->filter(function($refl) {
                return !is_null($refl);
            });
    }

    public static function getAllController(): Collection
    {
        return collect(File::allFiles(app_path('Http/Controllers')))
            ->map(function (SplFileInfo $splFileInfo) {
                $path = $splFileInfo->getRelativePathName();
                $namespace = sprintf('\%s%s', Container::getInstance()->getNamespace() . 'Http\Controllers\\',
                    strtr(substr($path, 0, strrpos($path, '.')), '/', '\\'));

                if (class_exists($namespace)) {
                    $reflection = new \ReflectionClass($namespace);
                    if ($reflection->isSubclassOf(Controller::class) && !$reflection->isAbstract()) {
                        return $reflection;
                    }
                }

                return null;
            })
            ->filter(function($refl) {
                return !is_null($refl);
            });
    }

    private function _getRouteNameByPath(\ReflectionMethod $method): ?string
    {
        $route = Route::getRoutes()->getByAction($method->class . '@' . $method->name);
        return $route ? $route->getName() : null;
    }

    public function _setModels(): self
    {
        /** @var \ReflectionClass $model */
        foreach ($this->models as $model) {
            $attr = $model->getAttributes(Attributes\LaraSwagger::class);
            if (empty($attr)) continue;

            /** @var Attributes\LaraSwagger $ls */
            $ls = $attr[0]->newInstance();
            $ls->tag = $ls->tag ?? $model->getShortName();
            $ls->controller = $ls->controller ?? $this->_getController($ls->tag . 'Controller');
            $ls->hidden = array_merge($ls->hidden, $this->_getProtectedValues($model, 'hidden', null));
            $ls->name = Str::plural(strtolower($ls->tag));
            $ls->filters = new Collection();
            $this->laraSwagger->push($ls);

            //read properties
            if (empty($ls->properties)) {
                $ls->properties = $this->_getDocProperties($model->getDocComment(), $ls);

                foreach ($model->getMethods() as $method) {
                    if ($method->getModifiers() == \ReflectionMethod::IS_PUBLIC &&
                        str_contains($method->class, $model->getName()) &&
                        $method->hasReturnType() &&
                        (str_starts_with($method->name, 'is') || str_starts_with($method->name, 'has'))
                    ) {
                        $ls->properties[$method->name] = $method->getReturnType()->getName();
                    }
                }

                $fillable = $this->_getProtectedValues($model, 'fillable');
                $ls->properties = array_merge($ls->properties, $fillable);
                $ls->properties = array_merge($ls->properties, $this->_getProtectedValues($model, 'casts'));
                $ls->properties = array_merge($ls->properties, $this->_getProtectedValues($model, 'dates', 'datetime'));
            }
            if (empty($ls->properties['id'])) $ls->properties = array_merge(['id' => 'int'], $ls->properties);

            //readonly
            if (!empty($fillable)) {
                $fillable = array_keys($fillable);
                foreach ($ls->properties as $property => $prop_type) {
                    if (!in_array($property, $fillable)) {
                        $ls->readOnly[] = $property;
                    }
                }
            }

            //unset hidden properties
            foreach ($ls->properties as $name => $type) {
                if (in_array($name, $ls->hidden)) {
                    unset($ls->properties[$name]);
                }
            }

            //filter params
            $filterParams = $model->getAttributes(Parameter::class);
            foreach ($filterParams as $param) {
                $ls->filters->push($param->newInstance());
            }

            $ls->initResources($this);
        }

        return $this;
    }

    public function _setControllers(): self
    {
        /** @var \ReflectionClass $controller */
        foreach ($this->controllers as $controller) {
            foreach ($controller->getMethods() as $method) {
                $op = $method->getAttributes(Operation::class);
                if (empty($op)) {
                    //override
                    $opTag = str_replace('Controller', '', $controller->getShortName());
                    $opName = strtolower(Str::plural($opTag));
                    switch ($method->getShortName()) {
                        case 'index':
                            $opEndpoint = '/' . $opName;
                            $opMethod = 'get';
                            break;
                        case 'store':
                            $opEndpoint = '/' . $opName;
                            $opMethod = 'post';
                            break;
                        case 'show':
                            $opEndpoint = '/' . $opName . '/{id}';
                            $opMethod = 'get';
                            break;
                        case 'update':
                            $opEndpoint = '/' . $opName . '/{id}';
                            $opMethod = 'put';
                            break;
                        case 'destroy':
                            $opEndpoint = '/' . $opName . '/{id}';
                            $opMethod = 'delete';
                            break;
                        default:
                            $opMethod = null;
                            $opEndpoint = null;
                    }
                    $ls = $this->operations->where('method',$opMethod)->where('endpoint', $opEndpoint)->first();
                } else {
                    /** @var Operation $op */
                    $op = $op[0]->newInstance();
                    $ls = $this->operations->where('method', $op->method)->where('endpoint', $op->endpoint)->first();

                    if (!$ls) {
                        $ls = ['method' => $op->method, 'endpoint' => $op->endpoint];
                        if ($op->tag) $ls['swagger']['tags'][] = $op->tag;
                        if ($op->description) $ls['swagger']['description'] = $op->description;
                        $ls['swagger']['summary'] = $op->summary ?? null;
                    }
                }

                if (empty($ls)) {
                    continue;
                }

                if ($this->laravel_route_name) {
                    $ls['swagger']['summary'] = $this->_getRouteNameByPath($method);
                }

                //read parameters
                $pars = $method->getAttributes(Parameter::class);
                foreach($pars as $par) {
                    $param = Attributes\LaraSwagger::getParameters($par->newInstance());

                    //remove duplicated
                    if (isset($ls['swagger']['parameters'])) {
                        foreach ($ls['swagger']['parameters'] as $index => $parameter) {
                            if ($parameter['name'] == $param['name'] && $parameter['in'] == $param['in']) {
                                unset($ls['swagger']['parameters'][$index]);
                                $ls['swagger']['parameters'] = array_values($ls['swagger']['parameters']);
                            }
                        }
                    }

                    $ls['swagger']['parameters'][] = $param;
                }

                //read response
                $resp = $method->getAttributes(Response::class);
                foreach ($resp as $res) {
                    $res = $res->newInstance();
                    /** @var Response $res */
                    $r = ['description' => $res->description];
                    if ($res->content) $r['content'] = $res->content;

                    if ($res->ref) $r = ['$ref' => $res->ref];
                    $ls['swagger']['responses'][$res->code] = $r;
                }

                //read request body
                $reqBody = $method->getAttributes(RequestBody::class);
                if (!empty($reqBody)) {
                    $body = new Collection();
                    foreach ($reqBody as $req) {
                        $body->push($req->newInstance());
                    }

                    $body = $body->groupBy('contentType');
                    foreach ($body as $contentType => $item) {
                        /** @var RequestBody $rb */
                        foreach ($item as $rb) {
                            if (empty($ls['swagger']['requestBody']['required'])) {
                                $ls['swagger']['requestBody']['required'] = $rb->required;
                            }

                            $ls['swagger']['requestBody']['content'][$rb->contentType]['schema']['type'] = 'object';
                            $ls['swagger']['requestBody']['content'][$rb->contentType]['schema']['properties'][$rb->name]['type'] = $rb->type;
                            $ls['swagger']['requestBody']['content'][$rb->contentType]['schema']['properties'][$rb->name]['description'] = $rb->desc;

                            if ($rb->example) {
                                if ($rb->type === 'array') {
                                    $ls['swagger']['requestBody']['content'][$rb->contentType]['schema']['properties'][$rb->name]['example'] = json_decode($rb->example);
                                } else {
                                    $ls['swagger']['requestBody']['content'][$rb->contentType]['schema']['properties'][$rb->name]['example'] = $rb?->example;
                                }
                            }

                            if ($rb->type == 'binary') {
                                $ls['swagger']['requestBody']['content'][$rb->contentType]['schema']['properties'][$rb->name]['type'] = 'string';
                                $ls['swagger']['requestBody']['content'][$rb->contentType]['schema']['properties'][$rb->name]['format'] = 'binary';
                            } else if ($rb->type == 'boolean') {
                                $ls['swagger']['requestBody']['content'][$rb->contentType]['schema']['properties'][$rb->name]['example'] = (bool) $rb->example;
                            }

                            if ($rb->required) {
                                $ls['swagger']['requestBody']['content'][$rb->contentType]['schema']['required'][] = $rb->name;
                            }
                        }
                    }
                }

                $this->operations->push($ls);
            }
        }

        return $this;
    }

    public function _getSchemas(): array
    {
        return $this->schemas->flatMap(function (array $item) {
            return [$item['index'] => $item['swagger']];
        })->toArray();
    }

    private function _getOperations(): array
    {
        $result = [];
        $this->operations->map(function (array $item) use (&$result) {
            if (is_callable($this->callbackOperation)) {
                $swagger = ($this->callbackOperation)($item['method'], $item['endpoint'], $item['swagger']);
            } else {
                $swagger = $item['swagger'];
            }

            if (is_array($swagger) && !empty($swagger)) {
                $result[$item['endpoint']][$item['method']] = $swagger;
            }
        });

//        debugbar()->debug($result);
        return $result;
    }

    public function _getResponses(): array
    {
        $this->responses->push(['index' => 'PlainText', 'swagger' => ['description' => 'Plain Text']]);
        $this->responses->push(['index' => 'Deleted', 'swagger' => ['description' => 'Deleted']]);
        $this->responses->push(['index' => 'BadRequest', 'swagger' => ['description' => 'Bad Request']]);
        $this->responses->push(['index' => 'Unauthorized', 'swagger' => ['description' => 'Unauthorized']]);
        $this->responses->push(['index' => 'Forbidden', 'swagger' => ['description' => 'Forbidden']]);
        $this->responses->push(['index' => 'NotFound', 'swagger' => ['description' => 'Not Found']]);
        $this->responses->push(['index' => 'NotAllowed', 'swagger' => ['description' => 'Not Allowed']]);
        $this->responses->push(['index' => 'ServerError', 'swagger' => ['description' => 'Server Error']]);

        return $this->responses->flatMap(function (array $item) {
            return [$item['index'] => $item['swagger']];
        })->toArray();
    }

    public function _getController(string $filename): ?string
    {
        /** @var \ReflectionClass $controller */
        foreach ($this->controllers as $controller) {
            if (str_ends_with($controller->getName(), $filename)) {
                return $controller->getName();
            }
        }

        return null;
    }

    public function _getDocProperties(?string $comment, Attributes\LaraSwagger $laraSwagger = null): array
    {
        $properties = [];
        preg_match_all('/(@property|@property-read) (\S+) (\S+)(.+)*$/m', $comment, $matches, PREG_SET_ORDER);
        foreach($matches as $match) {
            if (isset($match[3])) {
                $prop = (string) Str::of($match[3])->ltrim('$');
                $properties[$prop] = $match[2];

                if ($match[1] == '@property-read' && $laraSwagger) {
                    $laraSwagger->readOnly[] = $prop;
                }
            }
        }

//        debugbar()->debug($properties);
        return $properties;
    }

    public function _getProtectedValues(\ReflectionClass $refl, string $var, ?string $default = 'string'):array
    {
        $result = [];
        if ($refl->hasProperty($var)) {
            $prefl = $refl->getProperty($var);
            $prefl->setAccessible(true);
            $vals = $prefl->getValue($refl->newInstance());
            if (!empty($vals) && is_array($vals)) {
                if ($default && !Arr::isAssoc($vals)) {
                    foreach ($vals as $val) {
                        $result[$val] = $default;
                    }
                } else {
                    $result = $vals;
                }
            }
        }

        return $result;
    }
}
