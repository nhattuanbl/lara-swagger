<?php

namespace LaraSwagger;

use Illuminate\Support\ServiceProvider;

class LaraSwaggerServiceProvider extends ServiceProvider
{
    public function register()
    {
//        $this->mergeConfigFrom(__DIR__ . '/../config/lara-swagger.php', 'lara-swagger');

        $this->app->singleton(LaraSwagger::class, function($app) {
            return new LaraSwagger();
        });
    }

    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'lara_swagger');

        $this->publishes([
            __DIR__ . '/../resources/views' => resource_path('views/vendor/lara_swagger'),
        ], 'views');

        $this->publishes([
            __DIR__ . '/../resources/assets' => public_path('vendor/lara_swagger'),
        ], 'assets');
    }
}
